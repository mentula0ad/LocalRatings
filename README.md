# About LocalRatings

*LocalRatings* is a [0 A.D.](https://play0ad.com) mod aimed at ranking players, rating their performance and providing statistical data based on previously played games.

# The rating system

The rating system used by *LocalRatings* to evaluate players is explained [here](https://gitlab.com/mentula0ad/LocalRatings/-/blob/master/ABOUT.md).

Alternatively, you can open the `Local Ratings` menu page in 0 A.D. and click on `About Local Ratings`.

# Installation

[Click here](https://gitlab.com/mentula0ad/LocalRatings/-/releases/permalink/latest/downloads/localratings.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/LocalRatings/-/releases/permalink/latest/downloads/localratings.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/LocalRatings/-/releases/permalink/latest/downloads/localratings.zip) | [Older Releases](https://gitlab.com/mentula0ad/LocalRatings/-/releases)

# Questions & feedback

For more information, questions and feedback, visit the [thread on the 0 A.D. forum](https://wildfiregames.com/forum/topic/80151-localratings-mod-evaluate-players-skills-based-on-previous-games/).
