/**
 * This class helps replacing the SetupWindowPages.GameSetupPage class.
 * The purpose is to overwrite its methods, providing extra functionalities.
 * When created, we want a hotkey to be registered.
 */
class SetupWindowPagesGameSetupPage_LocalRatings extends SetupWindowPages.GameSetupPage {

    constructor(...args)
    {
        super(...args);

        // Register hotkey
        const button = new LocalRatingsButton();
        Engine.SetGlobalHotkey("localratings.dialog", "Press", button.onPress.bind(button));
    }

};

SetupWindowPages.GameSetupPage = SetupWindowPagesGameSetupPage_LocalRatings;
