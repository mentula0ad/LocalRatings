/**
 * This class helps replacing the PlayerAssignmentItem.Client class.
 * The purpose is to overwrite its methods, providing extra functionalities.
 * The createItem() method is responsible for displaying names, hence we want to modify it so that the rating is displayed next to each player's name.
 */
class PlayerAssignmentItemClient_LocalRatings extends PlayerAssignmentItem.Client {

    constructor(...args)
    {
        super(...args);
    }

    createItem(...args)
    {
        if (!global.g_LocalRatingsDatabase)
            return super.createItem(...args);

        // Add rating only if player is not new to the database
        const guid = args[0];
        const splittedPlayerName = getPlayerName_LocalRatings(g_PlayerAssignments[guid].name);
        const playerName = getCaseInsensitiveKey_LocalRatings(splittedPlayerName, g_LocalRatingsDatabase);
        if (!!playerName)
        {
            const playerData = g_LocalRatingsDatabase[playerName];
            return {
                "Handler": this,
                "Value": guid,
                "Autocomplete": g_PlayerAssignments[guid].name,
                "Caption": setStringTags(
                    addRating_LocalRatings(g_PlayerAssignments[guid].name, playerData),
                    g_PlayerAssignments[guid].player == -1 ? this.ObserverTags : this.PlayerTags)
            };
        }
        return super.createItem(...args);
    }

}

PlayerAssignmentItem.Client = PlayerAssignmentItemClient_LocalRatings;
