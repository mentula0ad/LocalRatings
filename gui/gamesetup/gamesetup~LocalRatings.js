// Store database in a global variable, for quick accessibility
var g_LocalRatingsDatabase;
if (Engine.ConfigDB_GetValue("user", "localratings.general.showmatchsetup") === "true")
{
    let ratingsDatabase = init_LocalRatings();
    let alias = new LocalRatingsAlias();
    alias.merge(ratingsDatabase, undefined);
    g_LocalRatingsDatabase = ratingsDatabase;
}
