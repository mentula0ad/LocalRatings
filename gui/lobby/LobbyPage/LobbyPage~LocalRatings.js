/**
 * This class helps replacing the LobbyPage class.
 * The purpose is to overwrite its methods, providing extra functionalities.
 * When created, we want a hotkey to be registered.
 */
class LobbyPage_LocalRatings extends LobbyPage {

    constructor(...args)
    {
        super(...args);

        // Register hotkey
        const button = new LocalRatingsButton();
        Engine.SetGlobalHotkey("localratings.dialog", "Press", button.onPress.bind(button));
    }

};

LobbyPage = LobbyPage_LocalRatings;
