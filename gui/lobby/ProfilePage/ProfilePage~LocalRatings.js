/**
 * This class helps replacing the ProfilePage class.
 * The purpose is to overwrite its methods, providing extra functionalities.
 * The onProfile() method is responsible for displaying a player's profile, hence we want to modify it so that the rating is displayed next to each player's name.
 */
class ProfilePage_LocalRatings extends ProfilePage
{

    constructor(...args)
    {
        super(...args);
    }

    onProfile(...args)
    {
        super.onProfile(...args);

        if (!global.g_LocalRatingsDatabase)
            return;

        // This is already run inside super.onProfile() but needs to be run again
	const attributes = Engine.GetProfile()[0];
	if (this.profilePage.hidden || this.requestedPlayer != attributes.player)
	    return;

	let profileFound = attributes.rating != "-2";
        if (!profileFound)
            return;

        const playerName = getCaseInsensitiveKey_LocalRatings(this.requestedPlayer, g_LocalRatingsDatabase);
        if (!!playerName)
            this.profilePlayernameText.caption = addRating_LocalRatings(this.profilePlayernameText.caption, g_LocalRatingsDatabase[playerName]);
    }

}

ProfilePage = ProfilePage_LocalRatings;
