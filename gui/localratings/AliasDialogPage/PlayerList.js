class LocalRatingsAliasDialogPagePlayerList
{

    constructor(aliasDialogPage)
    {
        // Arguments
        this.aliasDialogPage = aliasDialogPage;

        // GUI objects
        this.playerList = Engine.GetGUIObjectByName("playerList");

        // Events
        this.playerList.onSelectionChange = this.onSelectionChange.bind(this);
    }

    onSelectionChange()
    {
        this.aliasDialogPage.onSelectionChange();
    }

    getSelectedPlayer()
    {
        if (this.playerList.selected == -1)
            return;

        return this.playerList.list_playername[this.playerList.selected];
    }

    update()
    {
        // Construct table rows
        let rows = this.aliasDialogPage.players.map(x => [
            PlayerColor.ColorPlayerName(x, "", ""), // player
            x // playername
        ]);

        // Filter rows according to search entry
        const search = this.aliasDialogPage.getSearchCaption().toLowerCase();
        rows = rows.filter(x => x[1].toLowerCase().includes(search));

        // Sort rows
        rows.sort((a, b) => sortString_LocalRatings(a[1], b[1]));

        // Populate table with columns
        const columns = ["player", "playername"];
        columns.forEach((x, i) => this.playerList["list_" + x] = rows.map(y => y[i]));
        this.playerList.selected = -1;
        // Change these last, otherwise crash
        this.playerList.list = this.playerList.list_playername;
        this.playerList.list_data = this.playerList.list_playername;
    }

}
