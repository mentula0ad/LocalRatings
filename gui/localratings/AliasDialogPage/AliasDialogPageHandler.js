class LocalRatingsAliasDialogPage
{

    constructor(data)
    {
        // GUI objects
        this.textLabel = Engine.GetGUIObjectByName("textLabel");
        this.titleLabel = Engine.GetGUIObjectByName("titleLabel");
        this.cancelButton = Engine.GetGUIObjectByName("cancelButton");
        this.confirmButton = Engine.GetGUIObjectByName("confirmButton");

        // Events
        this.cancelButton.onPress = this.onPressCancelButton.bind(this);
        this.confirmButton.onPress = this.onPressConfirmButton.bind(this);

        this.searchPlayer = new LocalRatingsAliasDialogPageSearchPlayer(this);
        this.playerList = new LocalRatingsAliasDialogPagePlayerList(this);

        this.init(data);
    }

    onSearchEdit()
    {
        this.playerList.update();
    }

    onSelectionChange()
    {
        const player = this.playerList.getSelectedPlayer();
        this.confirmButton.enabled = !!player;
    }

    onPressCancelButton()
    {
        Engine.PopGuiPage();
    }

    onPressConfirmButton()
    {
        Engine.PopGuiPage({"player": this.playerList.getSelectedPlayer()});
    }

    getSearchCaption()
    {
        return this.searchPlayer.searchPlayerInput.caption;
    }

    init(data)
    {
        // Title
        if (data?.title)
            this.titleLabel.caption = translate(data.title);
        // Text
        if (data?.text)
            this.textLabel.caption = translate(data.text);
        // Player names
        if (data?.players)
            this.players = data.players;
        // Cancel button
        if (data?.cancel)
            this.cancelButton.caption = translate(data.cancel);
        else
            this.cancelButton.hidden = true;
        // Confirm button
        if (data?.confirm)
            this.confirmButton.caption = translate(data.confirm);
        else
            this.confirmButton.hidden = true;
        // Update
        this.playerList.update();
        this.onSelectionChange();
    }

}
