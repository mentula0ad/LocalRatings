async function init(data)
{

    let localRatingsPage = new LocalRatingsPage(data?.dialog);

    // Function to be called after a choice dialog is closed
    const onRebuildConfirmation = confirm => {
        if (!confirm)
            return;

        localRatingsPage.rebuildReplays();

        if (Object.keys(localRatingsPage.ratingsDatabase).length && Object.keys(localRatingsPage.historyDatabase).length)
            return;

        Engine.PushGuiPage("page_localratings_choicedialog.xml", {
            "title": "No replay found",
            "text": [
                "No replay was found.",
                "The player list will show up after your first game played.",
                "If you have already played one or more games, check your filters from the \"Options\" menu."
            ],
            "confirm": "Close"
        });
    };

    // Check if replay database is populated. If not, run the first run dialog.
    const replayDB = new LocalRatingsReplayDB();
    if (replayDB.isEmpty())
    {
        const data = await Engine.PushGuiPage("page_localratings_choicedialog.xml", {
            "title": "Welcome",
            "text": [
                "Here you will find the list of players, their rating and their statistics.",
                "The list is currently empty: click on \"Build list now\" to generate it (this may take a while)."
            ],
            "cancel": "Build list later",
            "confirm": "Build list now"
        });
        onRebuildConfirmation(data.confirm);
        return;
    }

    // Check if database version is the current one. If not, run the update required dialog.
    const cache = new LocalRatingsCache();
    if (cache.isUpdateRequired())
    {
        const data = await Engine.PushGuiPage("page_localratings_choicedialog.xml", {
            "title": "Update required",
            "text": [
                "A new version of the LocalRatings mod has been detected.",
                "The new version requires a rebuild of the player list: click on \"Rebuild list now\" to update (this may take a while)."
            ],
            "cancel": "Rebuild list later",
            "confirm": "Rebuild list now"
        });
        onRebuildConfirmation(data.confirm);
        return;
    }

    // All tests passed. Populate table
    localRatingsPage.updatePlayerList();

}
