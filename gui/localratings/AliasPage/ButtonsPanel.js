class LocalRatingsAliasPageButtonsPanel
{

    constructor(aliasPage)
    {
        // Arguments
        this.aliasPage = aliasPage;

        // GUI objects
        this.createButton = Engine.GetGUIObjectByName("createButton");
        this.deleteButton = Engine.GetGUIObjectByName("deleteButton");
        this.addButton = Engine.GetGUIObjectByName("addButton");
        this.removeButton = Engine.GetGUIObjectByName("removeButton");
        this.changeButton = Engine.GetGUIObjectByName("changeButton");
        this.closeButton = Engine.GetGUIObjectByName("closeButton");

        // Events
        this.createButton.onPress = this.onPressCreateButton.bind(this);
        this.deleteButton.onPress = this.onPressDeleteButton.bind(this);
        this.addButton.onPress = this.onPressAddButton.bind(this);
        this.removeButton.onPress = this.onPressRemoveButton.bind(this);
        this.changeButton.onPress = this.onPressChangeButton.bind(this);
        this.closeButton.onPress = this.onPressCloseButton.bind(this);
    }

    async onPressCreateButton()
    {
        const data = await Engine.PushGuiPage("page_localratings_aliasdialog.xml", {
            "title": "Create group",
            "text": translate("Select a player for the new group of aliases"),
            "players": this.aliasPage.players,
            "cancel": "Cancel",
            "confirm": "Create"
        });

        // If cancel button is hit,
        // do nothing
        if (!data || !("player" in data))
            return;

        let database = this.aliasPage.aliasesDatabase;
        const player = data.player;
        const primaryIdentities = Object.keys(database);
        const isPrimary = primaryIdentities.includes(player);

        // If the player is already a primary identity and has no aliases,
        // there's nothing to do
        if (isPrimary && !database[player].length)
            return;

        const aliasIdentities = Object.values(database).flat();
        const isAlias = aliasIdentities.includes(player);

        // If the player is not in any other group,
        // make a new group
        if (!isPrimary && !isAlias)
        {
            database[player] = [];
            this.aliasPage.setSelectedPrimary(player);
            this.aliasPage.update();
            return;
        }

        // If the player is in some other group,
        // show a dialog asking whether to remove it or not
        const dialogdata = await Engine.PushGuiPage("page_localratings_choicedialog.xml", {
            "title": "Player already in group",
            "text": [
                "The selected player is already in a group of aliases:",
                PlayerColor.ColorPlayerName(player, "", ""),
                "Move the player from their current group to the new group?"
            ],
            "cancel": "Cancel",
            "confirm": "Confirm"
        });

        // If the player should be kept in the old group,
        // do nothing
        if (!dialogdata.confirm)
            return;

        // If the player is a primary identity and has aliases,
        // remove it from that group, change the group primary identity and create a new group
        if (isPrimary)
        {
            const newPrimary = database[player][0];
            database[player].splice(0, 1);
            database[newPrimary] = database[player];
            database[player] = [];
            this.aliasPage.setSelectedPrimary(player);
            this.aliasPage.update();
            return;
        }

        // If the player is an alias,
        // remove it from that group and create a new group
        const primary = primaryIdentities.find(x => database[x].includes(player));
        const index = database[primary].indexOf(player);
        database[primary].splice(index, 1);
        database[player] = [];
        this.aliasPage.setSelectedPrimary(player);
        this.aliasPage.update();
    }

    async onPressDeleteButton()
    {
        const data = await Engine.PushGuiPage("page_localratings_choicedialog.xml", {
            "title": "Delete group",
            "text": [
                "Do you want to delete the selected group?"
            ],
            "cancel": "Cancel",
            "confirm": "Delete"
        });

        if (!data.confirm)
            return;

        let database = this.aliasPage.aliasesDatabase;
        const primary = this.aliasPage.getSelectedPrimary();

        delete database[primary];
        this.aliasPage.setSelectedPrimary();
        this.aliasPage.update();
    }

    async onPressAddButton()
    {
        let database = this.aliasPage.aliasesDatabase;
        const primary = this.aliasPage.getSelectedPrimary();

        const data = await Engine.PushGuiPage("page_localratings_aliasdialog.xml", {
            "title": "Add alias",
            "text": translate("Add a player to the group of aliases"),
            "players": this.aliasPage.players.filter(x => x !== primary && !database[primary].includes(x)),
            "cancel": "Cancel",
            "confirm": "Add"
        });

        // If cancel button is hit,
        // do nothing
        if (!data || !("player" in data))
            return;

        const player = data.player;

        // If the player is already in the group,
        // there's nothing to do
        if (player == primary || database[primary].includes(player))
            return;

        const primaryIdentities = Object.keys(database);
        const aliasIdentities = Object.values(database).flat();
        const isPrimary = primaryIdentities.includes(player);
        const isAlias = aliasIdentities.includes(player);

        // If the player is not in any other group,
        // add it to the group
        if (!isPrimary && !isAlias)
        {
            database[primary].push(player);
            database[primary].sort(sortString_LocalRatings);
            this.aliasPage.setSelectedPrimary(primary);
            this.aliasPage.update();
            return;
        }

        // If the player is in some other group,
        // show a dialog asking whether to remove it or not
        const dialogdata = await Engine.PushGuiPage("page_localratings_choicedialog.xml", {
            "title": "Player already in group",
            "text": [
                "The selected player is already in a group of aliases:",
                PlayerColor.ColorPlayerName(player, "", ""),
                "Move the player from their current group to the new group?"
            ],
            "cancel": "Cancel",
            "confirm": "Confirm"
        });

        // If the player should be kept in the old group,
        // do nothing
        if (!dialogdata.confirm)
            return;

        // If the player is a primary identity (of another group) and has no aliases,
        // delete that group and add the player to the new group
        if (isPrimary && !database[player].length)
        {
            delete database[player];
            database[primary].push(player);
            database[primary].sort(sortString_LocalRatings);
            this.aliasPage.setSelectedPrimary(primary);
            this.aliasPage.update();
            return;
        }

        // If the player is a primary identity (of another group) and has aliases,
        // remove it from that group, change the group primary identity and add it to the new group
        if (isPrimary)
        {
            const newPrimary = database[player][0];
            database[player].splice(0, 1);
            database[newPrimary] = database[player];
            delete database[player];
            database[primary].push(player);
            database[primary].sort(sortString_LocalRatings);
            this.aliasPage.setSelectedPrimary(primary);
            this.aliasPage.update();
            return;
        }

        // If the player is an alias,
        // remove it from that group and add it to the new group
        const oldPrimary = primaryIdentities.find(x => database[x].includes(player));
        const index = database[oldPrimary].indexOf(player);
        database[oldPrimary].splice(index, 1);
        database[primary].push(player);
        database[primary].sort(sortString_LocalRatings);
        this.aliasPage.setSelectedPrimary(primary);
        this.aliasPage.update();
    }

    async onPressRemoveButton()
    {
        let database = this.aliasPage.aliasesDatabase;
        const primary = this.aliasPage.getSelectedPrimary();

        const data = await Engine.PushGuiPage("page_localratings_aliasdialog.xml", {
            "title": "Remove alias",
            "text": translate("Remove a player from the group of aliases"),
            "players": [primary].concat(database[primary]),
            "cancel": "Cancel",
            "confirm": "Remove"
        });

        // If cancel button is hit,
        // do nothing
        if (!data || !("player" in data))
            return;

        const player = data.player;
        const isPrimary = player == primary;

        // If the player is a primary identity and has no aliases
        // delete the group
        if (isPrimary && !database[player].length)
        {
            delete database[player];
            this.aliasPage.setSelectedPrimary();
            this.aliasPage.update();
            return;
        }

        // If the player is a primary identity and has aliases
        // change the group primary identity and remove it from the group
        if (isPrimary)
        {
            const newPrimary = database[player][0];
            database[player].splice(0, 1);
            database[newPrimary] = database[player];
            delete database[player];
            this.aliasPage.setSelectedPrimary(newPrimary);
            this.aliasPage.update();
            return;
        }

        // If the player is an alias
        // remove it from the group
        const index = database[primary].indexOf(player);
        database[primary].splice(index, 1);
        this.aliasPage.setSelectedPrimary(primary);
        this.aliasPage.update();
    }

    async onPressChangeButton()
    {
        let database = this.aliasPage.aliasesDatabase;
        const primary = this.aliasPage.getSelectedPrimary();

        const data = await Engine.PushGuiPage("page_localratings_aliasdialog.xml", {
            "title": "Change primary",
            "text": translate("Select the primary identity for the group of aliases"),
            "players": database[primary],
            "cancel": "Cancel",
            "confirm": "Change"
        });

        // If cancel button is hit,
        // do nothing
        if (!data || !("player" in data))
            return;

        const player = data.player;

        // If the player is an alias
        // make it primary
        const index = database[primary].indexOf(player);
        database[primary].splice(index, 1);
        database[player] = database[primary];
        delete database[primary];
        database[player].push(primary);
        database[player].sort(sortString_LocalRatings);
        this.aliasPage.setSelectedPrimary(player);
        this.aliasPage.update();
    }

    onPressCloseButton()
    {
        Engine.PopGuiPage({"aliases": this.aliasPage.aliasesDatabase});
    }

}
