class LocalRatingsAliasPageSearchPlayer
{

    constructor(aliasPage)
    {
        // Arguments
        this.aliasPage = aliasPage;

        // GUI objects
        this.searchPlayerInput = Engine.GetGUIObjectByName("searchPlayerInput");

        // Events
        this.searchPlayerInput.onTextEdit = this.onSearchPlayerInputTextEdit.bind(this);
    }

    onSearchPlayerInputTextEdit()
    {
        this.aliasPage.setSelectedPrimary();
        this.aliasPage.updateGroupList();
    }

    getCaption()
    {
        return this.searchPlayerInput.caption;
    }

}
