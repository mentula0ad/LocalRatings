class LocalRatingsAliasPage
{

    constructor(players)
    {
        this.searchPlayer = new LocalRatingsAliasPageSearchPlayer(this);
        this.groupList = new LocalRatingsAliasPageGroupList(this);
        this.buttonsPanel = new LocalRatingsAliasPageButtonsPanel(this);

        this.players = players;
        this.aliasesDatabase = {};
        this.init();
    }

    onSelectionChange()
    {
        const primary = this.getSelectedPrimary();
        this.setSelectedPrimary(primary);

        this.buttonsPanel.deleteButton.enabled = !!primary;
        this.buttonsPanel.addButton.enabled = !!primary;
        this.buttonsPanel.removeButton.enabled = !!primary;
        this.buttonsPanel.changeButton.enabled = !!primary && this.aliasesDatabase[primary].length;
    }

    getSelectedPrimary()
    {
        return this.groupList.getSelectedPrimary();
    }

    setSelectedPrimary(player)
    {
        this.groupList.setSelectedPrimary(player);
    }

    updateGroupList()
    {
        this.groupList.setDatabase(this.aliasesDatabase);
        this.groupList.update();
    }

    update()
    {
        // Push to GUI
        this.updateGroupList();

        // Save aliases database
        let alias = new LocalRatingsAlias();
        alias.aliasesDatabase = this.aliasesDatabase;
        alias.save();
    }

    init()
    {
        // Load aliases database
        let alias = new LocalRatingsAlias();
        alias.load();
        this.aliasesDatabase = alias.aliasesDatabase;

        // Push to GUI
        this.updateGroupList();
    }

}
