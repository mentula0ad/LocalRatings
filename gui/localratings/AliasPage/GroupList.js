class LocalRatingsAliasPageGroupList
{

    constructor(aliasPage)
    {
        // Arguments
        this.aliasPage = aliasPage;

        // GUI objects
        this.groupList = Engine.GetGUIObjectByName("groupList");

        // Events
        this.groupList.onSelectionColumnChange = this.onSelectionColumnChange.bind(this);
        this.groupList.onSelectionChange = this.onSelectionChange.bind(this);

        // Other
        this.database = {};
        this.primary = -1;
        this.separator = " ∙ ";
        this.primaryIdentifier = " (" + translate("primary") + ")";
    }

    onSelectionColumnChange()
    {
        this.update();
    }

    onSelectionChange()
    {
        this.aliasPage.onSelectionChange();
    }

    getSelectedPrimary()
    {
        if (this.groupList.selected == -1)
            return;

        return this.groupList.list_primaryname[this.groupList.selected];
    }

    setSelectedPrimary(player)
    {
        this.primary = player;
    }

    setDatabase(database)
    {
        this.database = JSON.parse(JSON.stringify(database));
    }

    update()
    {
        // Construct table rows
        let rows = Object.keys(this.database).map(x => [
            [x].concat(this.database[x])
                .sort(sortString_LocalRatings)
                .map(y => (y == x) ? PlayerColor.ColorPlayerName(y, "", "") + this.primaryIdentifier : PlayerColor.ColorPlayerName(y, "", ""))
                .join(this.separator), // group
            x, // primary
            [x].concat(this.database[x])
                .sort(sortString_LocalRatings)
                .join(this.separator) // playernames
        ]);

        // Filter rows according to search entry
        const search = this.aliasPage.searchPlayer.getCaption().toLowerCase();
        rows = rows.filter(x => x[2].toLowerCase().includes(search));

        // Sort rows according to table preferences
        rows.sort((a, b) => sortString_LocalRatings(a[2], b[2]));

        // Populate table with columns
        const columns = ["group", "primaryname", "playernames"];
        columns.forEach((x, i) => this.groupList["list_" + x] = rows.map(y => y[i]));
        // Change these last, otherwise crash
        this.groupList.list = this.groupList.list_playernames;
        this.groupList.list_data = this.groupList.list_playernames;
        this.groupList.selected = this.groupList.list_primaryname.indexOf(this.primary);
    }

}
