class DynamicOptions
{

    constructor()
    {
        // GUI objects
        this.navigationBox = Engine.GetGUIObjectByName("navigationBox");
        this.optionControls = Engine.GetGUIObjectByName("option_controls");

        this.options = [];
        this.displayedOptions = [];
        this.currentPage = 0;
    }

    scroll(direction)
    {
        this.currentPage += direction;
        displayOptions();
    }

    display()
    {
        const size = this.options.length;
        const overflow = size > this.maxNumberOfOptions;

        // Push to GUI
        if (overflow)
        {
            // Display navigation box
            let optionControlsSize = this.optionControls.size;
            optionControlsSize.bottom = -80;
            this.optionControls.size = optionControlsSize;
            this.navigationBox.hidden = false;
            // Enable/Disable arrow buttons
            const maxPage = Math.floor(size/this.maxNumberOfOptions);
            let arrowLeft = Engine.GetGUIObjectByName("arrowLeft");
            let arrowRight = Engine.GetGUIObjectByName("arrowRight");
            arrowLeft.enabled = this.currentPage > 0;
            arrowRight.enabled = this.currentPage < maxPage;
            arrowLeft.onPress = this.scroll.bind(this, -1);
            arrowRight.onPress = this.scroll.bind(this, 1);
            arrowLeft.caption = "◀ " + translate("Previous");
            arrowRight.caption = translate("Next") + " ▶";
            // Set navigation text
            Engine.GetGUIObjectByName("navigationText").caption = sprintf(translate("Page %(currentPage)s of %(maxPage)s"), {"currentPage": this.currentPage+1, "maxPage": maxPage+1});
            // Set options
            const start = this.currentPage * this.maxNumberOfOptions;
            const end = overflow ? start + this.maxNumberOfOptions - 1 : size;
            g_Options[g_TabCategorySelected].options = this.options.slice(start, end);
        }
        else
            g_Options[g_TabCategorySelected].options = this.options;
    }

}

DynamicOptions.prototype.maxNumberOfOptions = 24;
