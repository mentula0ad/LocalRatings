class ModFilterOptions extends DynamicOptions
{

    constructor(...args)
    {
        super(...args);
        this.init();
    }

    init()
    {
        const cache = new LocalRatingsCache();
        const minifier = new LocalRatingsMinifier();
        const index = minifier.replayKeys.indexOf("mods");
        const minifiedReplayDatabase = cache.load("replayDatabase");
        const modList = Array.from(Object.values(minifiedReplayDatabase).map(x => x[index]).reduce((pv, cv) => cv = new Set([...pv, ...cv]), new Set()));
        const sortedList = modList.sort(sortString_LocalRatings);
        this.options = sortedList.map(mod =>
	    ({
                "type": "boolean",
                "label": mod,
                "tooltip": "Choose whether replays with this active mod must be considered for the rating calculation or not.",
                "config": "localratings.modfilter",
                "dynamic": mod.replace(" ", ""),
                "val": true
            })
        );
    }

    set()
    {
        for (let i=0; i<g_Options[g_TabCategorySelected].options.length; i++)
        {
            let option = g_Options[g_TabCategorySelected].options[i];
            let control = Engine.GetGUIObjectByName("option_control_" + option.type + "[" + i + "]");
            let optionType = g_OptionType[option.type];
            const configKey = option.config;
            const dynamic = option.dynamic;
            let value = !Engine.ConfigDB_GetValue("user", "localratings.modfilter")
                .split(" ")
                .filter(x => x !== "")
                .includes(dynamic);
            optionType.valueToGui(value, control);

            control[optionType.guiSetter] = function()
            {
                let value = optionType.guiToValue(control);
                if (optionType.sanitizeValue)
	            optionType.sanitizeValue(value, control, option);
                control.tooltip = option.tooltip + (optionType.tooltip ? "\n" + optionType.tooltip(value, option) : "");
                const hasChanges = Engine.ConfigDB_HasChanges("user");

                // Replace default setter with custom setter
                const oldValue = Engine.ConfigDB_GetValue("user", "localratings.modfilter");
                let dynamicList = oldValue.split(" ").filter(x => x !== "");
                const index = dynamicList.indexOf(dynamic);
                dynamicList =
                    (index > -1 && value) ? dynamicList.filter(x => x !== dynamic) :
                    (index == -1 && !value) ? dynamicList.concat([dynamic]) :
                    dynamicList;
                const newValue = dynamicList
                      .sort(sortString_LocalRatings)
                      .join(" ");
                Engine.ConfigDB_CreateValue("user", "localratings.modfilter", newValue);
                // End of replacement

                g_ChangedKeys.add(configKey);
                fireConfigChangeHandlers(new Set([configKey]));
                if (option.timeout)
	            optionType.timeout(option, oldValue, hasChanges, value);
                if (option.function)
	            Engine[option.function](value);
                enableButtons();
            }
        }
    }

}
