class LocalRatingsInfoButton
{

    constructor()
    {
        // GUI objects
        this.infoButton = Engine.GetGUIObjectByName("infoButton");

        // Events
        this.infoButton.onPress = this.onInfoButtonPress.bind(this);
    }

    onInfoButtonPress()
    {
        Engine.PushGuiPage("page_localratings_about.xml");
    }

}
