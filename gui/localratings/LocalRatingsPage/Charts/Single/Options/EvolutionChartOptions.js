/**
 * This class is responsible for reading the user-defined evolution chart options from the user.cfg configuraton file.
 */
class LocalRatingsEvolutionChartOptions
{

    constructor()
    {
        this.configOptions = {
            "showzero": this.getShowZero(),
            "showcurrent": this.getShowCurrent(),
            "showevolution": this.getShowEvolution(),
            "showperformance": this.getShowPerformance(),
            "showmarker": this.getShowMarker(),
            "colorzero": this.getColorZero(),
            "colorcurrent": this.getColorCurrent(),
            "colorevolution": this.getColorEvolution(),
            "colorperformance": this.getColorPerformance(),
            "colormarker": this.getColorMarker()
        }
    }

    getShowZero()
    {
        const showZeroValue = Engine.ConfigDB_GetValue("user", "localratings.charts.showzero");
        const showZeroValueBoolean = (showZeroValue === "true");
        return showZeroValueBoolean;
    }

    getShowCurrent()
    {
        const showCurrentValue = Engine.ConfigDB_GetValue("user", "localratings.charts.showcurrent");
        const showCurrentValueBoolean = (showCurrentValue === "true");
        return showCurrentValueBoolean;
    }

    getShowEvolution()
    {
        const showEvolutionValue = Engine.ConfigDB_GetValue("user", "localratings.charts.showevolution");
        const showEvolutionValueBoolean = (showEvolutionValue === "true");
        return showEvolutionValueBoolean;
    }

    getShowPerformance()
    {
        const showPerformanceValue = Engine.ConfigDB_GetValue("user", "localratings.charts.showperformance");
        const showPerformanceValueBoolean = (showPerformanceValue === "true");
        return showPerformanceValueBoolean;
    }

    getShowMarker()
    {
        const showMarkerValue = Engine.ConfigDB_GetValue("user", "localratings.charts.showmarker");
        const showMarkerValueBoolean = (showMarkerValue === "true");
        return showMarkerValueBoolean;
    }

    getColorZero()
    {
        const colorZeroValue = Engine.ConfigDB_GetValue("user", "localratings.charts.colorzero");
        const colorZeroValueColor = colorZeroValue + " 255";
        return colorZeroValueColor;
    }

    getColorCurrent()
    {
        const colorCurrentValue = Engine.ConfigDB_GetValue("user", "localratings.charts.colorcurrent");
        const colorCurrentValueColor = colorCurrentValue + " 255";
        return colorCurrentValueColor;
    }

    getColorEvolution()
    {
        const colorEvolutionValue = Engine.ConfigDB_GetValue("user", "localratings.charts.colorevolution");
        const colorEvolutionValueColor = colorEvolutionValue + " 255";
        return colorEvolutionValueColor;
    }

    getColorPerformance()
    {
        const colorPerformanceValue = Engine.ConfigDB_GetValue("user", "localratings.charts.colorperformance");
        const colorPerformanceValueColor = colorPerformanceValue + " 255";
        return colorPerformanceValueColor;
    }

    getColorMarker()
    {
        const colorMarkerValue = Engine.ConfigDB_GetValue("user", "localratings.charts.colormarker");
        const colorMarkerValueColor = colorMarkerValue + " 255";
        return colorMarkerValueColor;
    }

}
