/**
 * This class is responsible for reading the user-defined distribution chart options from the user.cfg configuraton file.
 */
class LocalRatingsDistributionChartOptions
{

    constructor()
    {
        this.configOptions = {
            "colorbinleft": this.getColorBinLeft(),
            "colorbinright": this.getColorBinRight(),
            "colorcurrentbin": this.getColorCurrentBin(),
            "histogrambins": this.getHistogramBins(),
            "showmean": this.getShowMean()
        }
    }

    getColorBinLeft()
    {
        const colorBinLeftValue = Engine.ConfigDB_GetValue("user", "localratings.distribution.colorleft");
        const colorBinLeftValueColor = colorBinLeftValue + " 255";
        return colorBinLeftValueColor;
    }

    getColorBinRight()
    {
        const colorBinRightValue = Engine.ConfigDB_GetValue("user", "localratings.distribution.colorright");
        const colorBinRightValueColor = colorBinRightValue + " 255";
        return colorBinRightValueColor;
    }

    getColorCurrentBin()
    {
        const colorCurrentBinValue = Engine.ConfigDB_GetValue("user", "localratings.distribution.colorcurrent");
        const colorCurrentBinValueColor = colorCurrentBinValue + " 255";
        return colorCurrentBinValueColor;
    }

    getHistogramBins()
    {
        const histogramBinsValue = Engine.ConfigDB_GetValue("user", "localratings.distribution.bins");
        const histogramBinsValueInt = +histogramBinsValue;
        return histogramBinsValueInt;
    }

    getShowMean()
    {
        const showMeanValue = Engine.ConfigDB_GetValue("user", "localratings.distribution.showmean");
        const showMeanValueBoolean = (showMeanValue === "true");
        return showMeanValueBoolean;
    }

}
