class LocalRatingsDistributionChart extends LocalRatingsChart
{

    constructor(...args)
    {
        super(...args);

        // GUI objects
        this.chartContainer = Engine.GetGUIObjectByName("distributionChartContainer");
        this.chartCanvas = Engine.GetGUIObjectByName("distributionChartCanvas");
        this.distributionMean = Engine.GetGUIObjectByName("distributionMeanLine");

        // Options
        this.configOptions = new LocalRatingsDistributionChartOptions().configOptions;

        this.ratingsList = args[2];
        this.playerRating;
        this.distributionBins;
        this.distributionCaps;
        this.poolSizes;
        this.init(args[2]);
    }

    init(ratingsDatabase)
    {
        this.ratingsList = Object.values(ratingsDatabase).map(x => x.rating).sort((a, b) => a-b);
        this.playerRating = ratingsDatabase[this.player].rating;
        this.poolSizes = this.getPoolSizes();
    }

    getPoolSizes()
    {
        const number = this.configOptions.histogrambins;
        const size = this.ratingsList.length;
        const min = this.ratingsList[0];
        const interval = (this.ratingsList[size-1] - min) / number;
        const findSplitterIndex = function (arr, value) {
            let a = 0;
            let b = arr.length - 1;
            while (a <= b)
            {
                const c = Math.floor((a+b)/2);
                if (arr[c] === value)
                    return c;
                else if (arr[c] < value)
                    a = c+1;
                else
                    b = c-1;
            }
            return a;
        }
        const indices = new Array(number).fill().map((x, i) => findSplitterIndex(this.ratingsList, min+i*interval)).concat([size]);
        const poolSizes = new Array(number).fill().map((x, i) => indices[i+1] - indices[i]);
        return poolSizes;
    }

    animate(...args)
    {
        if (this.distributionBins === undefined)
            return;

        const step = args[0];
        const maxPoolSize = Math.max(...this.poolSizes);

        let obj, size;

        for (let i=0; i<this.distributionBins.length; i++)
        {
            const height = 100*(1-step*this.poolSizes[i]/maxPoolSize);
            // Adjust bins
            obj = this.distributionBins[i];
            size = obj.size;
            size.rtop = height;
            obj.size = size;
            // Adjust caps
            obj = this.distributionCaps[i];
            size = obj.size;
            size.rtop = height;
            size.rbottom = height;
            obj.size = size;
        }

        obj = this.distributionMean;
        size = obj.size;
        size.rtop = 100*(1-step);
        obj.size = size;
    }

    draw(...args)
    {
        // Get info
        const canvasSize = this.chartCanvas.getComputedSize();
        const pools = this.poolSizes.length;
        const interval = (canvasSize.right - canvasSize.left) / pools;
        const maxPoolSize = Math.max(...this.poolSizes);
        const width = 100 / pools;
        const min = this.ratingsList[0];
        const max = this.ratingsList[this.ratingsList.length-1];
        const range = (max - min) / pools;

        let obj, size;

        // Color combination function
        const color1 = this.configOptions.colorbinleft.split(" ");
        const color2 = this.configOptions.colorbinright.split(" ");
        const colorCombination = (t) => color1.map((x, i) => Math.round((1-t)*x + t*color2[i])).join(" ");

        // GUI objects
        this.distributionBins = this.poolSizes.map((x, i) => Engine.GetGUIObjectByName("distributionBin[" + i + "]"));
        this.distributionCaps = this.poolSizes.map((x, i) => Engine.GetGUIObjectByName("distributionCap[" + i + "]"));
        this.distributionMean = Engine.GetGUIObjectByName("distributionMeanLine");

        // Set fixed data of bins
        for (let i=0; i<this.distributionBins.length; i++)
        {
            obj = this.distributionBins[i];
            size = obj.size;
            size.left = -interval/2;
            size.rleft = width/2 + width*i;
            size.right = -size.left;
            size.rright = size.rleft;
            obj.size = size;
            // Set colors
            obj.sprite = "color: " + colorCombination(i/pools);
            // Set tooltip
            const tooltipData = [
                translate("Tier") + ": " + (i+1) + " " + translate("of") + " " + pools,
                translate("Rating range") + ": " + formatRating_LocalRatings(min+range*i) + " " + translate("to") + " " + formatRating_LocalRatings(min+range*i+range-0.0001),
                translate("Players in this tier") + ": " + this.poolSizes[i] + " (" + (100 * this.poolSizes[i] / this.ratingsList.length).toFixed(2) + "%)"
            ];
            obj.tooltip = tooltipData.join("\n");
        }

        // Set fixed data of last bin
        const lastIndex = pools-1;
        const tooltipData = [
            translate("Tier") + ": " + pools + " " + translate("of") + " " + pools,
            translate("Rating range") + ": " + formatRating_LocalRatings(min+range*lastIndex) + " " + translate("to") + " " + formatRating_LocalRatings(min+range*lastIndex+range),
            translate("Players in this tier") + ": " + this.poolSizes[lastIndex] + " (" + (100 * this.poolSizes[lastIndex] / this.ratingsList.length).toFixed(2) + "%)"
        ];
        this.distributionBins[lastIndex].tooltip = tooltipData.join("\n");

        // Set fixed data of current player's bin
        const playerIndex = (this.playerRating == max) ? lastIndex : Math.floor((this.playerRating-min) / range);
        this.distributionBins[playerIndex].sprite = "color: " + this.configOptions.colorcurrentbin;

        // Set fixed data of caps
        for (let i=0; i<this.distributionCaps.length; i++)
        {
            obj = this.distributionCaps[i];
            size = obj.size;
            size.left = -interval/2;
            size.rleft = width/2 + width*i;
            size.right = -size.left;
            size.rright = size.rleft;
            obj.size = size;
        }

        // Add signals
        this.distributionBins.forEach(x => {
            x.onMouseEnter = (mouse) => x.sprite = x.sprite.slice(0, -3) + "200";
            x.onMouseLeave = (mouse) => x.sprite = x.sprite.slice(0, -3) + "255";
        });

        // Set mean indicator
        this.distributionMean.hidden = !this.configOptions.showmean;
        if (this.configOptions.showmean)
        {
            const mean = getMean_LocalRatings(this.ratingsList);
            for (let i=0; i<50; i++)
            {
                obj = Engine.GetGUIObjectByName("distributionMeanNotch[" + i + "]");
                size = obj.size;
                size.left = -1;
                size.right = -size.left;
                size.rleft = 100*(mean-min)/(max-min);
                size.rright = size.rleft;
                size.rbottom = 2*i;
                size.rtop = 2*i+1;
                obj.size = size;
            }

            // Set mean text
            obj = Engine.GetGUIObjectByName("distributionMeanText");
            size = obj.size;
            size.rleft = 100*(mean-min)/(max-min);
            size.rright = size.rleft;
            obj.size = size;
            obj.caption = (mean * 100).toFixed(2);
        }

        // Set legend
        const legend = [
            coloredText("■ " + translate("Low rating tiers"), this.configOptions.colorbinleft),
            coloredText("■ " + translate("High rating tiers"), this.configOptions.colorbinright),
            coloredText("■ " + translate("Player's tier"), this.configOptions.colorcurrentbin)
        ];
        this.chartLegend.caption = legend.join("   ");
        this.chartLegend.tooltip = translate("Individual graphs can be toggled and colors can be changed from the Options menu.");

        // Clean unused bins and caps
        for (let i=pools; i<100; i++)
        {
            obj = Engine.GetGUIObjectByName("distributionBin[" + i + "]");
            size = obj.size;
            size.rtop = 100;
            obj.size = size;
            obj = Engine.GetGUIObjectByName("distributionCap[" + i + "]");
            size = obj.size;
            size.rtop = 100;
            size.rbottom = 100;
            obj.size = size;
        };

        super.draw();
    }

}
