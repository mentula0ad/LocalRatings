class LocalRatingsChart
{

    constructor(player, playerData, ...args)
    {
        // GUI Objects
        this.chartLegend = Engine.GetGUIObjectByName("chartLegend");

        // Player information
        this.player = player;
        this.playerData = playerData;

        // Animation
        this.animationIntervals = 10;
        this.currentAnimation = 0;
    }

    onChartContainerTick(animatingFunction)
    {
        if (this.currentAnimation > this.animationIntervals)
        {
            delete this.chartContainer.onTick;
            return;
        }

        const step = this.currentAnimation / this.animationIntervals;
        this.animate(step);
        this.currentAnimation += 1;
    }

    animate(step)
    {
    }

    draw()
    {
        if (!this.chartContainer)
            return;

        this.chartContainer.hidden = false;
        this.chartContainer.onTick = this.onChartContainerTick.bind(this);
    }

    clear()
    {
        if (!this.chartContainer)
            return;

        this.chartContainer.hidden = true;
    }

}
