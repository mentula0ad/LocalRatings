class LocalRatingsSearchPlayer
{

    constructor(localRatingsPage)
    {
        // Arguments
        this.localRatingsPage = localRatingsPage;

        // GUI objects
        this.searchPlayerInput = Engine.GetGUIObjectByName("searchPlayerInput");

        // Events
        this.searchPlayerInput.onTextEdit = this.onSearchPlayerInputTextEdit.bind(this);
    }

    onSearchPlayerInputTextEdit()
    {
        this.localRatingsPage.updatePlayerList();
    }

    getCaption()
    {
        return this.searchPlayerInput.caption;
    }

    setCaption(caption)
    {
        this.searchPlayerInput.caption = caption;
    }

}
