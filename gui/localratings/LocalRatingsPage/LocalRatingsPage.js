class LocalRatingsPage
{

    constructor(dialog)
    {
        this.searchPlayer = new LocalRatingsSearchPlayer(this);
        this.playerList = new LocalRatingsPlayerList(this);
        this.playerProfile = new LocalRatingsPlayerProfile();
        this.infoButton = new LocalRatingsInfoButton();
        this.tabs = new LocalRatingsTabs(this);
        this.chartFrame = new LocalRatingsChartFrame();
        this.buttonsPanel = new LocalRatingsButtonsPanel(this);

        this.ratingsDatabase = {};
        this.historyDatabase = {};
        this.dialog = dialog;
	this.init();
    }

    init()
    {
        let ratingsDB = new LocalRatingsRatingsDB();
        ratingsDB.load();
        this.ratingsDatabase = ratingsDB.ratingsDatabase;
        this.historyDatabase = ratingsDB.historyDatabase;

        this.loadSettings();
        if (this.dialog)
            this.setDialogStyle();
    }

    onSelectionChange()
    {
        this.updateChartFrame();
        this.updatePlayerProfile();
    }

    rebuildReplays()
    {
        let replayDB = new LocalRatingsReplayDB();
        replayDB.rebuild();
        this.rebuildRatings();
    }

    rebuildRatings()
    {
        let ratingsDB = new LocalRatingsRatingsDB();
        ratingsDB.rebuild();
        ratingsDB.save();
        this.ratingsDatabase = ratingsDB.ratingsDatabase;
        this.historyDatabase = ratingsDB.historyDatabase;

        // Push to GUI
        this.updatePlayerList();
    }

    updatePlayerList()
    {
        let database = JSON.parse(JSON.stringify(this.ratingsDatabase));

        // Filter database according to player filters
        const filterObj = new LocalRatingsPlayerFilter(database);
        const filteredEntries = Object.entries(database).filter(([key, value]) => !filterObj.applies(key));
        database = Object.fromEntries(filteredEntries);

        // Merge aliases
        let alias = new LocalRatingsAlias();
        alias.merge(database, undefined);
        alias.removeDuplicates(database, undefined);

        // Push to GUI
        this.playerList.setDatabase(database);
        this.playerList.update();
    }

    updateChartFrame()
    {
        const player = this.playerList.getSelectedPlayer();
        if (!player)
        {
            this.chartFrame.hide();
            return;
        }

        // Merge aliases
        let historyDatabase = JSON.parse(JSON.stringify(this.historyDatabase));
        let alias = new LocalRatingsAlias();
        alias.merge(undefined, historyDatabase);

        const playerData = historyDatabase[player];
        const selectedTab = this.tabs.getSelectedTab();
        const database = this.playerList.getDatabase();

        // Push to GUI
        this.chartFrame.update(selectedTab, player, playerData, database);
    }

    updatePlayerProfile()
    {
        const player = this.playerList.getSelectedPlayer();
        const rank = this.playerList.getSelectedRank();
        const players = this.playerList.getNumberOfPlayers();
        if (!player || !rank)
        {
            this.playerProfile.hide();
            return;
        }

        // Merge aliases
        let historyDatabase = JSON.parse(JSON.stringify(this.historyDatabase));
        let alias = new LocalRatingsAlias();
        alias.merge(undefined, historyDatabase);

        const playerData = historyDatabase[player];

        // Push to GUI
        this.playerProfile.update(player, playerData, rank, players);
    }

    setDialogStyle() // See gui/lobby/LobbyPage/LobbyPage.js for inspiration
    {
        const windowMargin = 40;
        let obj, size;

        // Page
	obj = Engine.GetGUIObjectByName("page");
	obj.sprite = "ModernDialog";
	size = obj.size;
	size.left = windowMargin;
	size.top = windowMargin;
	size.right = -windowMargin;
	size.bottom = -windowMargin;
	obj.size = size;

        // Title
	obj = Engine.GetGUIObjectByName("pageTitle");
	size = obj.size;
	size.top -= windowMargin / 2;
	size.bottom -= windowMargin / 2;
	obj.size = size;

        // Panels
	obj = Engine.GetGUIObjectByName("panels");
	size = obj.size;
	size.top -= windowMargin / 2;
	obj.size = size;

        // Change buttons behavior
        this.buttonsPanel.setDialogStyle();
    }

    loadSettings()
    {
        const search = Engine.ConfigDB_GetValue("user", "localratings.save.search");
        const column = Engine.ConfigDB_GetValue("user", "localratings.save.column");
        const order = Engine.ConfigDB_GetValue("user", "localratings.save.order");

        this.searchPlayer.setCaption(search);
        this.playerList.setColumn(column);
        this.playerList.setColumnOrder(order);
    }

    saveSettings()
    {
        const search = this.searchPlayer.getCaption();
        const column = this.playerList.getColumn();
        const order = this.playerList.getColumnOrder();

        (search === "") ? Engine.ConfigDB_RemoveValue("user", "localratings.save.search") : Engine.ConfigDB_CreateValue("user", "localratings.save.search", search);
        (column === "rank") ? Engine.ConfigDB_RemoveValue("user", "localratings.save.column") : Engine.ConfigDB_CreateValue("user", "localratings.save.column", column);
        (order === 1) ? Engine.ConfigDB_RemoveValue("user", "localratings.save.order") : Engine.ConfigDB_CreateValue("user", "localratings.save.order", order);
        Engine.ConfigDB_SaveChanges("user");
    }

}
