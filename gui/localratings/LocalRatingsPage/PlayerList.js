class LocalRatingsPlayerList
{

    constructor(localRatingsPage)
    {
        // Arguments
        this.localRatingsPage = localRatingsPage;

        // GUI objects
        this.playerList = Engine.GetGUIObjectByName("playerList");

        // Events
        this.playerList.onSelectionColumnChange = this.onSelectionColumnChange.bind(this);
        this.playerList.onSelectionChange = this.onSelectionChange.bind(this);

        // Other
        this.database = {};
        this.columns = ["rank", "player", "rating", "matches", "playername"];
    }

    onSelectionColumnChange()
    {
        this.update();
    }

    onSelectionChange()
    {
        this.localRatingsPage.onSelectionChange();
    }

    getSelectedPlayer()
    {
        if (this.playerList.selected == -1)
            return;

        return this.playerList.list_playername[this.playerList.selected];
    }

    getSelectedRank()
    {
        if (this.playerList.selected == -1)
            return;

        return this.playerList.list_rank[this.playerList.selected];
    }

    getNumberOfPlayers()
    {
        return Object.keys(this.database).length;
    }

    getColumn()
    {
        return this.playerList.selected_column;
    }

    getColumnOrder()
    {
        return this.playerList.selected_column_order;
    }

    setColumn(column)
    {
        const defaultColumn = "rank";
        this.playerList.selected_column = this.columns.includes(column) ? column : defaultColumn;
    }

    setColumnOrder(order)
    {
        const defaultOrder = 1;
        const orderInt = !isNaN(order) ? +order : defaultOrder;
        this.playerList.selected_column_order = (orderInt === -1) ? -1 : defaultOrder;
    }

    getDatabase()
    {
        return this.database;
    }

    setDatabase(database)
    {
        this.database = JSON.parse(JSON.stringify(database));
    }

    update()
    {
        // Create an array of players, rating, matches.
        // We sort by rating, so we keep the information on their rank
        const items = Object.keys(this.database).map(x => [
            x,
            this.database[x].rating,
            this.database[x].matches
        ]).sort((a, b) => b[1] - a[1]);

        // Construct table rows, using the previously created items
        let rows = items.map((x, i) => [
            i+1, // rank
            PlayerColor.ColorPlayerName(x[0], "", ""), // player
            formatRating_LocalRatings(x[1]), // rating
            x[2], // matches
            x[0] // playername
        ]);

        // Filter rows according to search entry
        const search = this.localRatingsPage.searchPlayer.getCaption().toLowerCase();
        rows = rows.filter(x => x[4].toLowerCase().includes(search));

        // Sort rows according to table preferences
        const column = this.getColumn();
        const sortColumn = (a, b) => {
            return (column == "rank") ? b[0] - a[0] :
                (column == "rating") ? a[0] - b[0] :
                (column == "matches") && (b[3] === a[3]) ? b[0] - a[0] :
                (column == "matches") && (b[3] !== a[3]) ? b[3] - a[3] :
                sortString_LocalRatings(b[4], a[4]);
        };
        rows.sort(sortColumn);

        // Set order according to table preferences
        const order = this.getColumnOrder();
        if (order > 0)
            rows.reverse();

        // Populate table with columns
        this.columns.forEach((x, i) => this.playerList["list_" + x] = rows.map(y => y[i]));
        this.playerList.selected = -1;
        // Change these last, otherwise crash
        this.playerList.list = this.playerList.list_playername;
        this.playerList.list_data = this.playerList.list_playername;
    }

}
