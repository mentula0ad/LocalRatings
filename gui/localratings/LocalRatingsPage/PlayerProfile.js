class LocalRatingsPlayerProfile
{

    constructor()
    {
        // GUI objects
        this.playerProfile = Engine.GetGUIObjectByName("playerProfile");
    }

    hide()
    {
        this.playerProfile.hidden = true;
    }

    update(player, playerData, rank, players)
    {
        this.playerProfile.hidden = false;

        const singleGamesRatings = Object.keys(playerData)
              .sort()
              .map(x => playerData[x].rating);
        const averageRatings = singleGamesRatings
              .reduce((pv, cv, i) => pv.concat([pv[i] + cv]), [0])
              .slice(1)
              .map((x, i) => x / (i+1));
        const currentRating = averageRatings[averageRatings.length-1];
        const averageRating = getMean_LocalRatings(averageRatings);
        const ratingAverageDeviation = getAvd_LocalRatings(averageRatings, averageRating);
        const ratingStandardDeviation = getStd_LocalRatings(averageRatings, averageRating);
        const performanceAverageDeviation = getAvd_LocalRatings(singleGamesRatings, currentRating);
        const performanceStandardDeviation = getStd_LocalRatings(singleGamesRatings, currentRating);

        const captions = {
            "playerNameText": PlayerColor.ColorPlayerName(player, "", ""),
            "rankText": sprintf(translate("ranked %(rank)s of %(players)s players"), {"rank": rank, "players": players}),
            "currentRatingText": formatRating_LocalRatings(currentRating),
            "highestRatingText": formatRating_LocalRatings(Math.max(...averageRatings)),
            "lowestRatingText": formatRating_LocalRatings(Math.min(...averageRatings)),
            "averageRatingText": formatRating_LocalRatings(averageRating),
            "ratingAverageDeviationText": formatRating_LocalRatings(ratingAverageDeviation),
            "ratingStandardDeviationText": formatRating_LocalRatings(ratingStandardDeviation),
            "lastPerformanceText": formatRating_LocalRatings(singleGamesRatings[singleGamesRatings.length-1]),
            "bestPerformanceText": formatRating_LocalRatings(Math.max(...singleGamesRatings)),
            "worstPerformanceText": formatRating_LocalRatings(Math.min(...singleGamesRatings)),
            "averagePerformanceText": formatRating_LocalRatings(currentRating),
            "performanceAverageDeviationText": formatRating_LocalRatings(performanceAverageDeviation),
            "performanceStandardDeviationText": formatRating_LocalRatings(performanceStandardDeviation)
        };
        Object.entries(captions).forEach(([key, value]) => Engine.GetGUIObjectByName(key).caption = value);
    }

}
