class LocalRatingsChartFrame
{

    constructor()
    {
        // GUI objects
        this.chartContainer = Engine.GetGUIObjectByName("chartContainer");

        // Other
        this.chart;
    }

    hide()
    {
        this.chartContainer.hidden = true;
    }

    update(selectedTab, player, playerData, database)
    {
        this.chartContainer.hidden = false;

        if (this.chart)
            this.chart.clear();

        this.chart =
            (selectedTab == "evolution") ? new LocalRatingsEvolutionChart(player, playerData) :
            (selectedTab == "civilization") ? new LocalRatingsCivilizationChart(player, playerData) :
            (selectedTab == "distribution") ? new LocalRatingsDistributionChart(player, playerData, database) :
            undefined;

        if (this.chart)
            this.chart.draw();
    }

}
