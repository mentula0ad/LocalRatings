formatPlayerInfo = new Proxy(formatPlayerInfo, {apply: function(target, thisArg, args) {
    // Run original function
    const teams = target(...args);

    // If global ratings database is not defined, return
    // This happens, for example, in the replay menu page, where the rating shouldn't be displayed
    if (!global.g_LocalRatingsDatabase)
        return teams;

    // Split strings, add ratings and rejoin
    const players = teams
          .split("\n\n")
          .map(x => x.split("\n"));
    const playerNames = players
          .map(x => x.map(y => y.startsWith("[color=") ? y.substring(y.indexOf("]") + 1).split("[/color]")[0] : undefined)) // remove color
          .map(x => x.map(y => !!y && y.startsWith(g_BuddySymbol + " ") ? y.substring(g_BuddySymbol.length+1, y.length) : y)) // remove buddy symbol
          .map(x => x.map(y => !!y ? getPlayerName_LocalRatings(y) : y)) // remove rating
          .map(x => x.map(y => !!y ? getCaseInsensitiveKey_LocalRatings(y, g_LocalRatingsDatabase) : y)); // get name as in the database
    const ratings = playerNames.map(x => x.map(y => !!y ? addRating_LocalRatings("", g_LocalRatingsDatabase[y]) : undefined));
    const newTeams = players
          .map((x, i) => x.map((y, j) => ratings[i][j] ? y + ratings[i][j] : y))
          .map(x => x.join("\n"))
          .join("\n\n");

    return newTeams;
}});
