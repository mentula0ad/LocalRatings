/**
 * This class is responsible for updating or rebuilding the replay database stored in the cache folder.
 */
class LocalRatingsReplayDB
{

    constructor()
    {
        this.replayDatabase = {};
        this.newReplays = {};

        this.cache = new LocalRatingsCache();
        this.minifier = new LocalRatingsMinifier();
    }

    isEmpty()
    {
        return !Object.keys(this.cache.load("replayDatabase")).length;
    }

    load()
    {
        this.replayDatabase = this.minifier.magnifyReplayDatabase(this.cache.load("replayDatabase"));
    }

    save()
    {
        this.cache.save("replayDatabase", this.minifier.minifyReplayDatabase(this.replayDatabase));
    }

    rebuild()
    {
        Engine.GetReplays()
            .map(x => new LocalRatingsReplay(x))
            .filter(x => x.isValid)
            .forEach(x => this.replayDatabase[x.directory] = x);
        this.cache.updateVersion();
        this.save();
    }

    update()
    {
        this.load();

        // Update the replay database with new replays
        this.newReplays = Engine.GetReplays()
            .filter(x => !(x.directory in this.replayDatabase))
            .map(x => new LocalRatingsReplay(x))
            .filter(x => x.isValid)
            .reduce((pv, cv) => (pv[cv.directory] = cv, pv), {});
        if (!Object.keys(this.newReplays).length)
            return;

        Object.assign(this.replayDatabase, this.newReplays);
        this.save();
    }

}
