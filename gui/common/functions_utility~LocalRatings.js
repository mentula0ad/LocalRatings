/**
 * Convert a number to a string with two decimal digits
 * @param {number} value
 * @returns {string}
 */
function formatRating_LocalRatings(value)
{
    return (value * 100).toFixed(2);
}

/**
 * Return the mean of a non-empty numeric array
 * @param {array} array
 * @returns {number}
 */
function getMean_LocalRatings(array)
{
    return array.reduce((pv, cv) => pv + cv, 0) / array.length;
}

/**
 * Return the standard deviation of a non-empty numeric array with given mean value
 * @param {array} array
 * @param {number} mean
 * @returns {number}
 */
function getStd_LocalRatings(array, mean)
{
    return Math.sqrt(array.reduce((pv, cv) => pv + (cv-mean)**2, 0) / array.length);
}

/**
 * Return the average deviation of a non-empty numeric array with given mean value
 * @param {array} array
 * @param {number} mean
 * @returns {number}
 */
function getAvd_LocalRatings(array, mean)
{
    return getMean_LocalRatings(array.map(x => Math.abs(x - mean)));
}

/**
 * Return the name of a player, with the addition of the rating and the number of matches in gray color
 * @param {string} text
 * @param {object} playerData
 * @param {number} playerData.rating
 * @param {number} playerData.matches
 * @returns {string}
 */
function addRating_LocalRatings(text, playerData)
{
    const settings = new LocalRatingsSettings();
    const configKey = "localratings.general.format";
    const savedFormat = settings.getSaved()[configKey];
    const defaultFormat = settings.getDefault()[configKey];
    const format = !(/r.*r|m.*m/.test(savedFormat)) ? savedFormat : defaultFormat;
    const str = format
          .replace("r", formatRating_LocalRatings(playerData.rating))
          .replace("m", playerData.matches)
          .replace("\\", "\\\\") // escape backslash
          .replace("[", "\\[") // escape left square bracket
          .replace("]", "\\]") // escape right square bracket

    return text + " " + coloredText(str, "gray");
}

/**
 * splitRatingFromNick from gui/common/gamedescription.js does not adapt to situations like "Player One (custom rating)".
 * Therefore we need a different function to split the name from the rating.
 * @param {string} playerName
 * @returns {object}
 */
function getPlayerName_LocalRatings(playerName)
{
    const split = playerName.split(/\s+(?=\()/);
    return (split.length) ? split[0] : "";
}

/**
 * Return the key of the object that is case-insensitively equal to the string if found, undefined otherwise.
 * @param {string} string
 * @param {object} object
 * @returns {string|undefined}
 */
function getCaseInsensitiveKey_LocalRatings(string, object)
{
    return Object.keys(object).find(x => x.toLowerCase() === string.toLowerCase());
}

/**
 * Return -1 if a comes before b in the alphabet, +1 otherwise
 * @param {string} a
 * @param {string} b
 * @returns {number}
 */
function sortString_LocalRatings(a, b)
{
    return a.toLowerCase().localeCompare(b.toLowerCase());
}

/**
 * Return the ratings database, after performing an update with new replays
 * @returns {object}
 */
function init_LocalRatings()
{
    // Write default settings in user.cfg if not present
    const settings = new LocalRatingsSettings();
    settings.createDefaultSettingsIfNotExist();

    // If cache version has changed, we do nothing.
    // Only a full rebuild will update the cache version
    const cache = new LocalRatingsCache();
    if (cache.isUpdateRequired())
        return {};

    // Update the replay database with new replays and save ratings and history databases
    let replayDB = new LocalRatingsReplayDB();
    let ratingsDB = new LocalRatingsRatingsDB();

    replayDB.update();
    ratingsDB.load();
    if (Object.keys(replayDB.newReplays).length)
    {
        ratingsDB.merge(replayDB.newReplays);
        ratingsDB.save();
    }

    return ratingsDB.ratingsDatabase;
}
