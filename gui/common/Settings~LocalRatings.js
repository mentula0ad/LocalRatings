/**
 * This class is responsible for dealing with settings.
 */
class LocalRatingsSettings
{

    getSaved()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/localratings/OptionsPage/options.json");
        let settings = {};
        optionsJSON.forEach(category => category.options.forEach(option => settings[option.config] = Engine.ConfigDB_GetValue("user", option.config)));
        settings = Object.assign(settings, this.getDynamicSaved());
        return settings;
    }

    getDynamicSaved()
    {
        return {"localratings.modfilter" : Engine.ConfigDB_GetValue("user", "localratings.modfilter")};
    }

    getDefault()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/localratings/OptionsPage/options.json");
        let settings = {};
        optionsJSON.forEach(category => category.options.forEach(option => settings[option.config] = option.val.toString()));
        settings = Object.assign(settings, this.getDynamicDefault());
        return settings;
    }

    getDynamicDefault()
    {
        return {"localratings.modfilter" : ""};
    }

    createDefaultSettingsIfNotExist()
    {
        const settings = this.getDefault();
        Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
        Engine.ConfigDB_SaveChanges("user");
        return settings;
    }

}
