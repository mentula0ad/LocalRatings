/**
 * This class is responsible for interactions with cache files, like loading or saving.
 * It is able to detect whether the database structure has changed, due to installation of a new version of the mod.
 */
class LocalRatingsCache
{

    constructor()
    {
        const version = Engine.GetEngineInfo().mods.filter(x => x.mod == "public")[0].version;
        const path = "moddata/localratings/";
        this.replayDatabaseFile = path + version + "/replayDatabase.json";
        this.ratingsDatabaseFile = path + version + "/ratingsDatabase.json";
        this.historyDatabaseFile = path + version + "/historyDatabase.json";
        this.aliasesDatabaseFile = path + version + "/aliasesDatabase.json";
        this.cacheVersionFile = path + version + "/cacheVersion.json";

        this.createCacheFilesIfNotExist();
    }

    createCacheFilesIfNotExist()
    {
        [
            this.replayDatabaseFile,
            this.ratingsDatabaseFile,
            this.historyDatabaseFile,
            this.aliasesDatabaseFile
        ]
            .filter(x => !Engine.FileExists(x))
            .forEach(x => Engine.WriteJSONFile(x, {}));
    }

    tagToFilename(tag)
    {
        return (tag == "replayDatabase") ?
            this.replayDatabaseFile :
            (tag == "ratingsDatabase") ?
            this.ratingsDatabaseFile :
            (tag == "historyDatabase") ?
            this.historyDatabaseFile :
            (tag == "aliasesDatabase") ?
            this.aliasesDatabaseFile :
            "";
    }

    updateVersion()
    {
        Engine.ProfileStart("LocalRatingsCacheUpdateVersion");
        Engine.WriteJSONFile(this.cacheVersionFile, {"version": this.version});
	Engine.ProfileStop();
    }

    isUpdateRequired()
    {
        return !Engine.FileExists(this.cacheVersionFile) || Engine.ReadJSONFile(this.cacheVersionFile).version !== this.version;
    }

    load(tag)
    {
	Engine.ProfileStart("LocalRatingsLoadCacheFile");
        const file = this.tagToFilename(tag);
	const data = Engine.FileExists(file) && Engine.ReadJSONFile(file);
	Engine.ProfileStop();
        return (data) ? data : {};
    }

    save(tag, json)
    {
	Engine.ProfileStart("LocalRatingsSaveCacheFile");
        const file = this.tagToFilename(tag);
	Engine.WriteJSONFile(file, json);
	Engine.ProfileStop();
    }

}

LocalRatingsCache.prototype.version = 5;
