/**
 * This class helps replacing the Menu class.
 * The purpose is to overwrite its methods.
 * The getHandlerNames() method is responsible for displaying menu buttons in order.
 */
class Menu_LocalRatings extends Menu {

    constructor(...args)
    {
        super(...args);
    }

    getHandlerNames(...args)
    {
        // Remove the LocalRatings button
        let handlerNames = super.getHandlerNames(...args);
        let index = handlerNames.indexOf("LocalRatings");
        handlerNames.splice(index, 1);

        // If option is disabled, register hotkey and return
        const show = Engine.ConfigDB_GetValue("user", "localratings.general.showmenubutton");
        if (show !== "true")
        {
            const button = new MenuButtons.prototype.LocalRatings();
            Engine.SetGlobalHotkey("localratings.dialog", "Press", button.onPress.bind(button));
            return handlerNames;
        }

        // Try to put the button after the "Lobby" button.
        // This condition will always be triggered, but we allow
        // other cases for pure compatibility with other mods
        index = handlerNames.indexOf("Lobby");
        if (index > -1)
        {
            handlerNames.splice(index+1, 0, "LocalRatings");
            return handlerNames;
        }
        // Try to put the button after the "Hotkeys" button
        index = handlerNames.indexOf("Hotkeys");
        if (index > -1)
        {
            handlerNames.splice(index+1, 0, "LocalRatings");
            return handlerNames;
        }
        // No luck, we put the button as first
        handlerNames.splice(0, 0, "LocalRatings");
        return handlerNames;
    }

}

Menu = Menu_LocalRatings;
